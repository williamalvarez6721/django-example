# Please note this is a snapshot of all the whole service and not the whole service.

#### ⛔ Do not try to run this code as it will not work because there are not variables called in the environment. This is just a showcase.
</br>

# Wallet user service
This is a service we are using in order to mae a multiservice application with k8b. This wallet service is responsible to gather and save information about one user's wallet. Their wallet address, stake keys, userID, etc.

The front end will use this service as a Restful API, where it can POST, GET and DELETE (we have reasons why we don't allow them to PUT/PATCH).

Please ask me any question on how this would work.

William.