from email import message
from pydoc_data.topics import topics
import re
from urllib import response
from bson import ObjectId
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import NotFound

import requests


from functools import wraps
import jwt

from django.http import JsonResponse

from rest_framework.decorators import api_view

from .models import WalletUser, Wallet_List
from .serializer import WalletsUserSerializer, Owned_Wallet_Serializer, CreateWalletUserSerializer


# AUTH0 (DO NOT CHANGE)
def get_token_auth_header(request):
    """Obtains the Access Token from the Authorization Header
    """
    auth = request.META.get("HTTP_AUTHORIZATION", None)
    parts = auth.split()
    token = parts[1]

    return token

def requires_scope(required_scope):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
    """
    def require_scope(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            token = get_token_auth_header(args[0])
            decoded = jwt.decode(token, verify=False)
            if decoded.get("scope"):
                token_scopes = decoded["scope"].split()
                for token_scope in token_scopes:
                    if token_scope == required_scope:
                        return f(*args, **kwargs)
            response = JsonResponse({'message': 'You don\'t have access to this resource'})
            response.status_code = 403
            return response
        return decorated
    return require_scope



class WalletsListView(APIView):
    def get(self, _request):
        user_wallet = WalletUser.objects.all()
        serialized_items = (WalletsUserSerializer(user_wallet, many=True))
        return Response(serialized_items.data, status=status.HTTP_200_OK)


    def post(self, request):
        new_wallet = CreateWalletUserSerializer(data=request.data)
        if new_wallet.is_valid():
            new_wallet.save()
            return Response(new_wallet.data, status=status.HTTP_201_CREATED)
        return Response(new_wallet.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        

class WalletDetailedView(APIView):
    def get_wallet_user(self, pk):
        try: 
            return WalletUser.objects.get(pk=pk)
        except WalletUser.DoesNotExist:
            raise NotFound(detail="Not found")
        
    def get(self, _request, pk):
        profile_no= self.get_wallet_user(pk=pk)
        serialized_profile= WalletsUserSerializer(profile_no)
        return Response(serialized_profile.data, status=status.HTTP_200_OK)

    def post(self, request, pk):
        try:
            wallet_to_post = self.get_wallet_user(pk)
            updated_wallet = WalletsUserSerializer(wallet_to_post, data=request.data)
            if updated_wallet.is_valid():
                updated_wallet.save()
                return Response(updated_wallet.data, status=status.HTTP_202_ACCEPTED)
            return Response(updated_wallet.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except WalletUser.DoesNotExist:
            raise NotFound(detail="Not found")


class WalletOwnedView(APIView):
    def get(self, _request):
        user_wallet = Wallet_List.objects.all()
        serialized_items = (Owned_Wallet_Serializer(user_wallet, many=True))
        return Response(serialized_items.data, status=status.HTTP_200_OK)

    def post(self, request):
            new_wallet = Owned_Wallet_Serializer(data=request.data)
            if new_wallet.is_valid():
                new_wallet.save()
                return Response(new_wallet.data, status=status.HTTP_201_CREATED)
            return Response(new_wallet.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)


class WalletOwnedDetailedView(APIView):
    def get_wallet_user_pk(self, pk):
        Wallet_List._id = ObjectId(pk)
        try: 
            return Wallet_List.objects.get(pk=Wallet_List._id)
        except WalletUser.DoesNotExist:
            raise NotFound(detail="Not found")

    def get(self, _request, pk):
        Wallet_List._id = ObjectId(pk)
        profile_no= self.get_wallet_user_pk(pk=Wallet_List._id)
        serialized_profile= Owned_Wallet_Serializer(profile_no)
        return Response(serialized_profile.data, status=status.HTTP_200_OK)

    def delete(self, _request, pk):
        Wallet_List._id = ObjectId(pk)
        wallet_to_delete = self.get_wallet_user_pk(pk=Wallet_List._id)
        wallet_to_delete.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
