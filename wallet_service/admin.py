from django.contrib import admin

from .models import WalletUser
from .models import Wallet_List
 
admin.site.register(WalletUser)
admin.site.register(Wallet_List)
