
from djongo import models



class WalletUser(models.Model):
    user_id = models.CharField(max_length=1000, default='', unique=True, primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
       return f"{self.user_id}"

class Wallet_List(models.Model):
    _id = models.ObjectIdField()
    wallet_addr = models.CharField(max_length=2000, default='')
    wallet_stake = models.CharField(max_length=2000, default='')
    wallet_validated = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(
    to=WalletUser,
    related_name='wallets_owned',
    on_delete=models.CASCADE
    )

   

    def __str__(self):
     return f"{self.wallet_addr}"

