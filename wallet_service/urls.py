from django import views
from django.urls import path
from .views import WalletsListView, WalletDetailedView, WalletOwnedView, WalletOwnedDetailedView



urlpatterns = [
    path('', WalletsListView.as_view()), # This will show all users
    path('w/', WalletOwnedView.as_view()), # This will show all the wallet from every user but not user info.
    path('w/<pk>', WalletOwnedDetailedView.as_view()), # This will show one specific wallet by using the owner/Auth0 id.
    path('<pk>/', WalletDetailedView.as_view()) # This will show the user information for a user, including wallet list.
    

]