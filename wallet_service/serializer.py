# from django.db.models import fields
from rest_framework import serializers
from .models import WalletUser, Wallet_List

class Owned_Wallet_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet_List
        fields= ("__all__")


class CreateWalletUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = WalletUser
        fields= ("__all__")

class WalletsUserSerializer(serializers.ModelSerializer):
    wallets_owned = Owned_Wallet_Serializer(many=True)  
    class Meta:
        model = WalletUser
        fields= ("__all__")
